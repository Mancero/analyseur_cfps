bach for Max
Version 0.7.9 beta

- SYSTEM REQUIREMENTS
- HOW TO INSTALL
- HOW TO UNINSTALL
- WHERE TO START
- ACKNOWLEDGMENTS
- CONTACT

===================================================
SYSTEM REQUIREMENTS
===================================================

Intel-based Mac, OSX 10.6 or higher, running Max 6.1.7 or higher.
Windows XP, Vista or 7, running Max 6.1.7 or higher.


===================================================
HOW TO INSTALL
===================================================

You should install bach directly from Max Package Manager.

Alternatively, you can copy the "bach" folder inside the "packages" folder in your Max folder.
The Max packages folder is under "Documents/Max" (for Max 6) or "Documents/Max 7" (for Max 7).


===================================================
HOW TO UNINSTALL
===================================================

Uninstall from Max Package Manager, or delete the "bach" folder from the "packages" folder.


===================================================
WHERE TO START
===================================================

The patch "bach.overview" (located in the "Extras" Max's menu) contains a detailed documentation of all the global system features (which you should read entirely), and links to the help files of all the externals and abstractions. We have tried to keep all this information in a somewhat pedagogical order, and in principle you are encouraged to run through it from beginning to end to get an idea of the tools you are provided with.

The best thing to do, to start learning bach, is following the tutorials that you find in the "bach.overview". 

The only prerequisite to bach is a solid (but not necessarily hyper-advanced) knowledge of Max. The documentation will sometimes hint at advanced topics, such as threading, but if you don't feel at ease with them you can simply overlook them.

Of course, experimentation is always the core of understanding. As soon as you get the very first notions, the best you can do is start patching. Just remember that this is still an alpha release: save your patches and data as often as you can - we can't assure you that Max won't hang or crash as soon as you click that button you just added!


===================================================
ACKNOWLEDGMENTS
===================================================

This project has been partially undertaken during the permanence of the authors at IRCAM, first as students at the Cursus in Composition and Musical Informatics and then as composers in research.

We wish to thank: Carlos Agon, Arshia Cont, Eric Daubresse, Emmanuel Jourdan, Serge Lemouton, Jean Lochard, Mikhail Malt and all the other people at IRCAM who have supported and encouraged the work, although not an internal IRCAM project.

We also wish to thank: Sara Adhitya, Davide Bordogna, DaFact, Phil Gaskill, Eric Grunin, Mika Kuuskankare, Andrew Pask, Andrea Rota, Andrea Sarto.

bach includes the Kiss FFT library (distributed under BSD licence), the Mini-XML library (distributed under LGPLv2.0 license), the IRCAM SDIF library (distributed under LGPLv2.1 license), and the GPC library (free for non commercial use). 


===================================================
CONTACT
===================================================

To contact us send a mail to info@bachproject.net. For bug report and discussions, you can also use the forum on our website: www.bachproject.net.

